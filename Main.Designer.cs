﻿namespace WebServer
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_http = new System.Windows.Forms.Button();
            this.button_php = new System.Windows.Forms.Button();
            this.button_mysql = new System.Windows.Forms.Button();
            this.button_openweb = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.richTextBox_logs = new System.Windows.Forms.RichTextBox();
            this.label_http = new System.Windows.Forms.Label();
            this.label_php = new System.Windows.Forms.Label();
            this.label_mysql = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_http
            // 
            this.button_http.Location = new System.Drawing.Point(6, 19);
            this.button_http.Name = "button_http";
            this.button_http.Size = new System.Drawing.Size(137, 44);
            this.button_http.TabIndex = 0;
            this.button_http.Text = "HTTP Сервер";
            this.button_http.UseVisualStyleBackColor = true;
            this.button_http.Click += new System.EventHandler(this.button_http_Click);
            // 
            // button_php
            // 
            this.button_php.Location = new System.Drawing.Point(6, 70);
            this.button_php.Name = "button_php";
            this.button_php.Size = new System.Drawing.Size(137, 47);
            this.button_php.TabIndex = 1;
            this.button_php.Text = "PHP Сервер";
            this.button_php.UseVisualStyleBackColor = true;
            // 
            // button_mysql
            // 
            this.button_mysql.Location = new System.Drawing.Point(6, 123);
            this.button_mysql.Name = "button_mysql";
            this.button_mysql.Size = new System.Drawing.Size(137, 47);
            this.button_mysql.TabIndex = 2;
            this.button_mysql.Text = "MySQL Сервер";
            this.button_mysql.UseVisualStyleBackColor = true;
            this.button_mysql.Click += new System.EventHandler(this.button_mysql_Click);
            // 
            // button_openweb
            // 
            this.button_openweb.Location = new System.Drawing.Point(6, 176);
            this.button_openweb.Name = "button_openweb";
            this.button_openweb.Size = new System.Drawing.Size(137, 47);
            this.button_openweb.TabIndex = 3;
            this.button_openweb.Text = "Открыть папку с проектом";
            this.button_openweb.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_mysql);
            this.groupBox1.Controls.Add(this.label_php);
            this.groupBox1.Controls.Add(this.label_http);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(419, 233);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Состояние сервера";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_http);
            this.groupBox2.Controls.Add(this.button_php);
            this.groupBox2.Controls.Add(this.button_mysql);
            this.groupBox2.Controls.Add(this.button_openweb);
            this.groupBox2.Location = new System.Drawing.Point(438, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 234);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Панель управления";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(26, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "PHP Сервер : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(18, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "HTTP Сервер : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(6, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "MySQL Сервер : ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.richTextBox_logs);
            this.groupBox4.Location = new System.Drawing.Point(10, 81);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(403, 141);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Логи сервера";
            // 
            // richTextBox_logs
            // 
            this.richTextBox_logs.Location = new System.Drawing.Point(7, 20);
            this.richTextBox_logs.Name = "richTextBox_logs";
            this.richTextBox_logs.Size = new System.Drawing.Size(390, 110);
            this.richTextBox_logs.TabIndex = 0;
            this.richTextBox_logs.Text = "";
            // 
            // label_http
            // 
            this.label_http.AutoSize = true;
            this.label_http.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_http.ForeColor = System.Drawing.Color.Red;
            this.label_http.Location = new System.Drawing.Point(137, 18);
            this.label_http.Name = "label_http";
            this.label_http.Size = new System.Drawing.Size(88, 20);
            this.label_http.TabIndex = 4;
            this.label_http.Text = "Выключен";
            // 
            // label_php
            // 
            this.label_php.AutoSize = true;
            this.label_php.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_php.ForeColor = System.Drawing.Color.Red;
            this.label_php.Location = new System.Drawing.Point(137, 38);
            this.label_php.Name = "label_php";
            this.label_php.Size = new System.Drawing.Size(88, 20);
            this.label_php.TabIndex = 5;
            this.label_php.Text = "Выключен";
            // 
            // label_mysql
            // 
            this.label_mysql.AutoSize = true;
            this.label_mysql.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_mysql.ForeColor = System.Drawing.Color.Red;
            this.label_mysql.Location = new System.Drawing.Point(137, 58);
            this.label_mysql.Name = "label_mysql";
            this.label_mysql.Size = new System.Drawing.Size(88, 20);
            this.label_mysql.TabIndex = 6;
            this.label_mysql.Text = "Выключен";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 256);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Simple WebServer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_http;
        private System.Windows.Forms.Button button_php;
        private System.Windows.Forms.Button button_mysql;
        private System.Windows.Forms.Button button_openweb;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label_mysql;
        private System.Windows.Forms.Label label_php;
        private System.Windows.Forms.Label label_http;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RichTextBox richTextBox_logs;
        private System.Windows.Forms.Label label3;
    }
}

