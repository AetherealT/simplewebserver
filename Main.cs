﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using weblib;
using System.Diagnostics;

namespace WebServer
{
    public partial class Main : Form
    {       
        public Main()
        {
            InitializeComponent();
        }

        public int http_flag = 0;

        private void button_http_Click(object sender, EventArgs e)
        {
            http_flag += 1;

            Process iStartProcess = new Process();
            iStartProcess.StartInfo.FileName = "http.exe";
            iStartProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                 
            if(http_flag == 1)
            {
                richTextBox_logs.Text += "HTTP Сервер запущен" + Environment.NewLine;
                button_http.Text = "Остановить HTTP";
                iStartProcess.Start();
                label_http.ForeColor = Color.Green;
                label_http.Text = "Включен";
            }

            if(http_flag == 2)
            {                
                Process[] listprosecc = Process.GetProcesses();
                button_http.Enabled = false;
                foreach (Process oneproc in listprosecc)
                {
                    string ProsessName = oneproc.ProcessName;

                    try
                    {
                        foreach (Process proc in Process.GetProcessesByName("http"))
                        {
                            proc.Kill();
                            richTextBox_logs.Text += "HTTP Сервер остановлен" + Environment.NewLine;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    label_http.ForeColor = Color.Red;
                    label_http.Text = "Выключен";                    
                    button_http.Text = "HTTP Сервер";
                    button_http.Enabled = true;
                }

                http_flag = 0;
            }

        }

        public int mysql_flag = 0;

        private void button_mysql_Click(object sender, EventArgs e)
        {
            mysql_flag += 1;
            string config;
            config = File.ReadAllText("mysql.ini");
            Process iStartProcess = new Process();
            iStartProcess.StartInfo.FileName = "cmd";
            iStartProcess.StartInfo.Arguments = @"@/k mysql\bin\mysql.exe " + config;
            //iStartProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            if (mysql_flag == 1)
            {
                richTextBox_logs.Text += "MySQL Сервер запущен" + Environment.NewLine;
                button_mysql.Text = "Остановить MySQL";
                iStartProcess.Start();
                label_mysql.ForeColor = Color.Green;
                label_mysql.Text = "Включен";
            }

            if (mysql_flag == 2)
            {
                button_mysql.Enabled = false;

                try
                {
                    foreach (Process proc in Process.GetProcessesByName("mysql"))
                    {
                        proc.Kill();
                        foreach (Process proc_cmd in Process.GetProcessesByName("cmd"))
                        {
                            proc_cmd.Kill();                            
                        }

                        richTextBox_logs.Text += "MySQL Сервер остановлен" + Environment.NewLine;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                label_mysql.ForeColor = Color.Red;
                label_mysql.Text = "Выключен";
                button_mysql.Text = "MySQL Сервер";
                button_mysql.Enabled = true;

                mysql_flag = 0;
            }


        }


    }
}
